#!/usr/bin/python

import datetime
import json
import logging
import os
import re
import sys

#conc_keywords = ["thread","concurrent","parallel","future","atomic","run","sync","task"]
conc_keywords = ["thread","concurrent","parallel","future","atomic","runnable","sync"]

def checkListForConcKeyword(searchList,path):
    for elem in searchList:
        for key in conc_keywords:
            if key in elem.lower():
               print "{};{}".format(path,elem)
               #return 1
    return 0

def checkLibsConcCPP(libs,path):
    return checkListForConcKeyword(libs,path)

def checkLibsConcCSharp(libs,path):
     return checkListForConcKeyword(libs,path)

def checkLibsConcJava(libs,path):
    return checkListForConcKeyword(libs,path)

def checkLambdaConcCPP(libs,path):
    return checkListForConcKeyword(lambd,path)

def checkLambdaConcCSharp(libs,path):
    return checkListForConcKeyword(lambd,path)

def checkLambdaConcJava(libs,path):
    return checkListForConcKeyword(lambd,path)

def writeFileOutputLine(project, lang, jsonData):
    for srcFile in jsonData['allLambdas']:
        concLibs = 0 
        concNode = 0
        lambdas = []
        numInfo = 0
        if lang=="java":
            concLibs = checkLibsConcJava(srcFile['info']['lib'],srcFile['file'])
        if lang=="cpp":
            concLibs = checkLibsConcCPP(srcFile['info']['lib'],srcFile['file'])
        if lang=="cs":
            concLibs = checkLibsConcCPP(srcFile['info']['lib'],srcFile['file'])

logger = logging.getLogger('log')
logger.setLevel(logging.INFO)
chLog = logging.StreamHandler()
#fhLog = logging.FileHandler("log_{}_{}.log".format(str(datetime.datetime.now()).replace(" ","").replace(":","-").replace(".","-"),os.path.basename(__file__)))
logger.addHandler(chLog)
#logger.addHandler(fhLog)
if(len(sys.argv) < 3):
     logger.warn("usage {} <jsonFilePath> <[cpp|cs|java]>".format(os.path.basename(__file__)))
     quit()

jsonFilePath = sys.argv[1]
lang = sys.argv[2]

projectRegex =  re.compile(r"(^|.*/)([^/]+)\.json")
project = ""
match = projectRegex.match(jsonFilePath)
if match:
    project = match.group(2)
count=0
with open(jsonFilePath) as jsonFile:    
    for jsonLine in jsonFile:
        count += 1
        project += "{}".format(count)
        jsonData = json.loads(jsonLine)
        writeFileOutputLine(project,lang,jsonData) 
