#!/bin/bash

line=$1
parse=$2
path=$3
regex="name=([^;]+);url=(.+)"
if [[ $line =~ $regex ]]
then
    name="${BASH_REMATCH[1]}"
    url="${BASH_REMATCH[2]}"
    echo "clone $name ...."
    git clone $url "$path/$name"
    java -jar $path/parserCollection.jar -c $2 "$path/$name/" >> "$path/$name.json"
    echo "${#name}"
    if [ ${#name} > 2 ]
      then
        rm -r $path/$name
    fi
else
    echo "I'm singing in the rain..."
fi
