#!/bin/bash

echo "convert csv-files to file lists"

grep -Eo "([^/]+\.java)" allFilesJava.csv > allFilesJava.txt
echo "allFilesJava.txt written"
grep -Eo "([^/]+\.java)" allLambdaFilesJava.csv > allLambdaFilesJava.txt
echo "allLambdaFilesJava.txt written"
grep -Eo "([^/]+\.cs)" allFilesCs.csv > allFilesCs.txt
echo "allFilesCs.txt written"
grep -Eo "([^/]+\.cs)" allLambdaFilesCs.csv > allLambdaFilesCs.txt
echo "allLambdaFilesCs.txt written"
grep -Eo "([^/]+\.(cpp|hpp|CPP|HPP|cp|cc|cxx|c\+\+))" allFilesCpp.csv > allFilesCpp.txt
echo "allFilesCpp.txt written"
grep -Eo "([^/]+\.(cpp|hpp|CPP|HPP|cp|cc|cxx|c\+\+))" allLambdaFilesCpp.csv > allLambdaFilesCpp.txt
echo "allLambdaFilesCpp.txt written"
