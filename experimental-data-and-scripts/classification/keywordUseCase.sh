#!/bin/bash

file=$1

cat $file | sort > tmp
cat tmp > $file

echo "All Files"
wc -l $file
rm *$file.files


#Network
echo "Network"
cat $file | grep -iE "net|server|client|remote|middleman|http|chat|distributed|delegate|request|timeout|message|share|sharing|socket|connect|receive|send|traffic|cookie|connection|cloud|ping|session|download|web|host|url|dns" > "Network$file.files"
wc -l "Network$file.files"
#Multi-Threading
echo "Multi-Threading"
cat $file | grep -iE "thread|parallel|concurrent|sync|block|schedule|wait|handle|runtime|semaphore|event|signal|task|callback|callable|dispatch|mutex|suspend|job|sleep|timer" > "Threading$file.files"
wc -l "Threading$file.files"

#Testing
echo "Test"
cat $file | grep -iE "test|benchmark|assert" > "Test$file.files"
wc -l "Test$file.files"

#Logging
echo "Logging"
cat $file | grep -iE "log" > "Logging$file.files"
wc -l "Logging$file.files"

#UI/GUI
echo "GUI/UI"
cat $file | grep -iE "gui|widget|view|shell|display|button|canvas|editor|console|popup|bar|frame|scrollbar|window|about|listener|menu|command|action|layout|box|frontend" > "GUI$file.files"
wc -l "GUI$file.files"

#Exception-Handling
echo "Error-Handling"
cat $file | grep -iE "exception|error|warn|crash|bug|check|undefined" > "ErrorHandling$file.files"
wc -l "ErrorHandling$file.files"

#Composition
echo "Composition"
cat $file | grep -iE "composition|spawn|multiplexer|serializer|compose|split|join|build|combine|combinator" > "Composition$file.files"
wc -l "Composition$file.files"

#Configuration
echo "Configuration"
cat $file | grep -iE "config|setting|info|readme|version|option|default" > "Configuration$file.files"
wc -l "Configuration$file.files"

#File-Handling
echo "FileHandling"
cat $file | grep -iE "file|directory|writer|reader" > "FileHandling$file.files"
wc -l "FileHandling$file.files"

#graphics
echo "Graphical"
cat $file | grep -iE "graphical|color|pixel|gpu|render|light|texture|image|picture|shadow|draw|animation|animate|fade|geometryi|point|scene|line|polygon|square|rectangle|circle|shape|visualisize|visualizer|ellipse|paint" > "Graphics$file.files"
wc -l "Graphics$file.files"

#multimedia
echo "Multimedia"
cat $file | grep -iE "audio|video|media|player|touch|virtual|recognize|gesture|codec" > "Multimedia$file.files"
wc -l "Multimedia$file.files"

#Algorithmic
echo "Algorithmic"
cat $file | grep -iE "algorithm|calc|search|script|analyze|util|helper|manager|cluster|generate|generation|generator|optimize|optimization|visitor|process|select|service|factory|provider|operate|operation|operator|find|tracker|mutation|mutate|mutator|compute|backend|sort" > "Algorithmic$file.files"
wc -l "Algorithmic$file.files"

#Security
echo "Security"
cat $file | grep -iE "security|authenticate|authentication|account|crypt" > "Security$file.files"
wc -l "Security$file.files"

#Datastructure
echo "Datastructure"
cat $file | grep -iE "array|list|map|tree|string|object|graph|heap|stack|table|queue|node|template|matrix|vector|collect|set|item|unit|type|entity|enum|container|element|model" > "Datastructure$file.files"
wc -l "Datastructure$file.files"

#Embedded
echo "Embedded"
cat $file | grep -iE "hardware|camera|keyboard|mouse|memory|embedded|device|cpu|x86|x64" > "Embedded$file.files"
wc -l "Embedded$file.files"

#TextManagement
echo "TextManagement"
cat $file | grep -iE "string|character|document|parser|parse|csv|json|xml|scan|format|translation|translate|encoding|encode|decoding|decode|utf8|ascii|word" > "TextManagement$file.files"
wc -l "TextManagement$file.files"

#Database
echo "Data-Storage"
cat $file | grep -iE "data|storage|cache|sql|query|queries" > "DataStorage$file.files"
wc -l "DataStorage$file.files"


cat *$file.files | sort > tmp
comm -23 $file tmp > "Misc$file.files"

echo "Miscellaneous"
wc -l "Misc$file.files"
