mkdir C++
cp parserCollection.jar C++
head -n 1000 analyzed_projects/C++.log | xargs -i{} ./gitClone.sh {} cpp C++/

mkdir C#
cp parserCollection.jar C#
head -n 1000 analyzed_projects/C#.log | xargs -i{} ./gitClone.sh {} cpp C#/

mkdir Java
cp parserCollection.jar Java
head -n 1000 analyzed_projects/Java.log | xargs -i{} ./gitClone.sh {} cpp Java/
