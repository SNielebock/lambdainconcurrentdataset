#!/bin/bash
path=$1/*
projLog=$2
regexFile=".*/([^/]+)\.json"
regexProj="name=([^;]+);url=(.+)*"

jsonFiles=()

for file in $path
do
    if [[ $file =~ $regexFile ]]
    then
        projName="${BASH_REMATCH[1]}"
        jsonFiles=("${jsonFiles[@]}" "$projName")
    fi
done

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ $line =~ $regexProj ]]
    then
        projNameLog="${BASH_REMATCH[1]}"
        projURLLog="${BASH_REMATCH[2]}"
        # check if name is in array
        exist=0
        for i in "${jsonFiles[@]}"
        do
            #echo "compare $i and $projNameLog"
            if [ "$i" == "$projNameLog" ]
            then                
                exist=1
                break
            fi
        done
        if [ $exist = 0 ] 
        then
            echo "name=$projNameLog;url=$projURLLog"
        fi
    fi
done < "$projLog"
