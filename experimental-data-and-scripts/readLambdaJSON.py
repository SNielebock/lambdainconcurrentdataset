#!/usr/bin/python

import datetime
import json
import logging
import os
import re
import sys

#conc_keywords = ["thread","concurrent","parallel","future","atomic","run","sync","task"]
conc_keywords = ["thread","concurrent","parallel","future","atomic","runnable","sync"]

def checkListForConcKeyword(searchList):
    for elem in searchList:
        for key in conc_keywords:
            if key in elem.lower():
                return 1
    return 0

def checkLibsConcCPP(libs):
    return checkListForConcKeyword(libs)

def checkLibsConcCSharp(libs):
     return checkListForConcKeyword(libs)

def checkLibsConcJava(libs):
    return checkListForConcKeyword(libs)

def checkLambdaConcCPP(lambd):
    return checkListForConcKeyword(lambd)

def checkLambdaConcCSharp(lambd):
    return checkListForConcKeyword(lambd)

def checkLambdaConcJava(lambd):
    return checkListForConcKeyword(lambd)

def writeFileOutputLine(project, lang, jsonData):
    for srcFile in jsonData['allLambdas']:
        concLibs = 0 
        concNode = 0
        lambdas = []
        numInfo = 0
        if lang=="java":
            concLibs = checkLibsConcJava(srcFile['info']['lib'])
        if lang=="cpp":
            concLibs = checkLibsConcCPP(srcFile['info']['lib'])
        if lang=="cs":
            concLibs = checkLibsConcCSharp(srcFile['info']['lib'])
        lambdas = srcFile['info']['lambdas']
        if srcFile['info']['numberLambdas']==0:
            print "{},{},{},{},{},{},{}".format(project, lang, srcFile['file'].replace(",","-"),concLibs,0,concNode,"False")
        # if the script fails due to a missing field in the json file this is the location to fix that - trans lambdaNodes to lamdaNodea -_-
        for lambdaNodes in lambdas:
            if lang=="java":
                concNode = checkLambdaConcJava(lambdaNodes['nodes'])
            if lang=="cpp":
                concNode = checkLambdaConcCPP(lambdaNodes['nodes'])
                if numInfo>0 and lambdaNodes['hasCapturedVariables']:
                    additionalVal = 1
                    numInfo -= 1
                else:
                    additionalVal = 0
            if lang=="cs":
                concNode = checkLambdaConcCSharp(lambdaNodes['nodes'])
            print "{},{},{},{},{},{},{}".format(project, lang, srcFile['file'].replace(",","-"),concLibs,1,concNode,lambdaNodes['hasCapturedVariables'])

logger = logging.getLogger('log')
logger.setLevel(logging.INFO)
chLog = logging.StreamHandler()
#fhLog = logging.FileHandler("log_{}_{}.log".format(str(datetime.datetime.now()).replace(" ","").replace(":","-").replace(".","-"),os.path.basename(__file__)))
logger.addHandler(chLog)
#logger.addHandler(fhLog)
if(len(sys.argv) < 3):
     logger.warn("usage {} <jsonFilePath> <[cpp|cs|java]>".format(os.path.basename(__file__)))
     quit()

jsonFilePath = sys.argv[1]
lang = sys.argv[2]

projectRegex =  re.compile(r"(^|.*/)([^/]+)\.json")
project = ""
match = projectRegex.match(jsonFilePath)
if match:
    project = match.group(2)
count=0
with open(jsonFilePath) as jsonFile:    
    for jsonLine in jsonFile:
        count += 1
        project += "{}".format(count)
        jsonData = json.loads(jsonLine)
        writeFileOutputLine(project,lang,jsonData) 
