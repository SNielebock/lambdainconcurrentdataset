#!/bin/bash

jsonPath=$1
lang=$2

echo "project,lang,file,containsConcLib,numberLambda,containsConcNode,isClosure,additionalInfo"
ls $jsonPath/*.json | xargs -i{} python readLambdaJSONWithAdditional.py {} $lang
