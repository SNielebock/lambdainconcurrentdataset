The code and dataset is covered by the CRAPL licence (see CRAPL-LICENSE.txt)

# LambdaInConcurrentDataSet

This repository provides the data sets and scripts used in the paper "Programmers do not Favor Lambda Expressions for Concurrent Object-Oriented Code" by Sebastian Nielebock, Robert Heumüller, and Frank Ortmeier from the Chair of Software Engineering - Faculty of Computer Science of the Otto-von-Guericke University Magdeburg, Germany published in Springer's Empirical Software Engineering (https://doi.org/10.1007/s10664-018-9622-9) on May 2nd, 2018..

You find a post-peer-review, pre-copyedit version of the article on our webpage https://cse.cs.ovgu.de/cse/programmers-do-not-favor-lambda-expressions-for-concurrent-object-oriented-code-4/

All scripts and data sets were provided by Sebastian Nielebock and come without guarantee. When using the scripts and data sets please refer to the README file within the directory experimental-data-and-scripts. For any issues regarding replication do not hesitate to contact me (sebastian.nielebock <at> ovgu.de)

If you use or refer to these datasets, please cite our paper by using the following BibTex entry.

<at>article{NielebockLambda2018,
author="Nielebock, Sebastian
and Heum{\"u}ller, Robert
and Ortmeier, Frank",
title="Programmers do not Favor Lambda Expressions for Concurrent Object-Oriented Code",
journal="Empirical Software Engineering",
year="2018",
month="May",
day="02",
abstract="Lambda expressions have long been state-of-the-art in the functional programming paradigm. Especially with regard to the use of higher-order functions, they provide developers with a means of defining predicate or projection functions locally, which greatly increases the comprehensibility of the resulting source code. This benefit has motivated language designers to also incorporate lambda expressions into object-oriented (OO) programming languages. In particular, they are claimed to facilitate concurrent programming. One likely reason for this assumption is their purity: pure lambda expressions are free of side effects, and therefore cannot cause, e.g., race conditions. In this paper, we present the first empirical analysis of whether or not this claim is true for OO projects. For this purpose, we investigated the application of lambda expressions in 2923 open-source projects, implemented in one of the most common OO programming languages: C{\#}, C++, and Java. We present three major findings. First, the majority of lambda expressions are not applied in concurrent code and most concurrent code does not make use of lambda expressions. Second, for all three of the languages, we observed that developers compromise their code by applying a significantly higher number of impure, capturing lambda expressions, which are capable of causing race conditions. Finally, we explored further use cases of lambda expressions and found out that testing, algorithmic implementation, and UI are far more common use-cases for the application of lambda expressions. Our results encourage to investigate in more detail the reasons that hinder programmers to apply lambda expressions in concurrent programming and to support developers, e.g., by providing automatic refactorings.",
issn="1573-7616",
doi="10.1007/s10664-018-9622-9",
url="https://doi.org/10.1007/s10664-018-9622-9"
}

